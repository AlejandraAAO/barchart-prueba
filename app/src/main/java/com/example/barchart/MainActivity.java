package com.example.barchart;

import androidx.appcompat.app.AppCompatActivity;

import android.graphics.Color;
import android.os.Bundle;

import com.github.mikephil.charting.charts.BarChart;
import com.github.mikephil.charting.data.BarData;
import com.github.mikephil.charting.data.BarDataSet;
import com.github.mikephil.charting.data.BarEntry;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity {

    BarChart barChart;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        barChart = findViewById(R.id.bar_chart);

        BarDataSet barDataSet1 = new BarDataSet(dataValues1(), "DataSet 1");
        BarDataSet barDataSet2 = new BarDataSet(dataValues2(),"DataSet2");

        barDataSet1.setColors(Color.rgb(161, 196, 199));
        barDataSet1.setValueTextSize(10);
        barDataSet2.setColors(Color.rgb(186, 152, 212));
        barDataSet2.setValueTextSize(10);

        BarData barData = new BarData();
        barData.addDataSet(barDataSet1);
        barData.addDataSet(barDataSet2);

        barChart.setData(barData);
        barChart.invalidate();

    }
    private ArrayList<BarEntry> dataValues1(){
        ArrayList<BarEntry> dataVals = new ArrayList<>();
        dataVals.add(new BarEntry(0,3));
        dataVals.add(new BarEntry(1,4));
        dataVals.add(new BarEntry(2,6));
        dataVals.add(new BarEntry(3,11));
        dataVals.add(new BarEntry(4,16));
        return dataVals;
    }

    private ArrayList<BarEntry> dataValues2(){
        ArrayList<BarEntry> dataVals = new ArrayList<>();
        dataVals.add(new BarEntry(1.8f,2));
        dataVals.add(new BarEntry(2,8));
        dataVals.add(new BarEntry(3.6f,3));
        dataVals.add(new BarEntry(1.8f,7));
        return dataVals;
    }
}
